﻿using AXLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryTest
{
    class Program
    {
        static void Main(string[] args)
        {
            AXParser parser = new AXParser();
            Expression expression = parser.Parse("pow(x,(2-3+3.5*z))");

            ExpressionContext context = new ExpressionContext();

            context.Bind(Variable.x, 4);
            context.Bind(Variable.z, 1);

            Console.WriteLine(expression.Evaluate(context).ToString());

            Console.ReadLine();
        }
    }
}
