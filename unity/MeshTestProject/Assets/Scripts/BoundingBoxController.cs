﻿using System;
using UnityEngine;

public class BoundingBoxController : MonoBehaviour
{
    [Header("Components")]
    public GameObject tagPrefab;
    public GameObject linePrefab;
    public Collider boundingBox;

    [Header("Display values")]
    internal int[] minmax;
    public float tagSpacing;

    /// <summary>
    /// Set up bounding box
    /// </summary>
    public void SetUp()
    {
        SetUpTags();
        SetUpAxis();
    }

    /// <summary>
    /// Set up axis labels
    /// </summary>
    private void SetUpAxis()
    {
        GameObject line1 = Instantiate(linePrefab, transform);
        LineController lc = line1.GetComponent<LineController>();
        lc.points = new Vector3[] { 
                                    boundingBox.bounds.min,
                                    new Vector3(boundingBox.bounds.max.x, boundingBox.bounds.min.y, boundingBox.bounds.min.z),
                                    new Vector3(boundingBox.bounds.max.x, boundingBox.bounds.max.y, boundingBox.bounds.min.z),
                                    new Vector3(boundingBox.bounds.min.x, boundingBox.bounds.max.y, boundingBox.bounds.min.z),
                                    boundingBox.bounds.min
                                  };
        lc.SetUp();

        GameObject line2 = Instantiate(linePrefab, transform);
        LineController lc2 = line2.GetComponent<LineController>();
        lc2.points = new Vector3[] { 
                                     boundingBox.bounds.max,
                                     new Vector3(boundingBox.bounds.min.x, boundingBox.bounds.max.y, boundingBox.bounds.max.z),
                                     new Vector3(boundingBox.bounds.min.x, boundingBox.bounds.min.y, boundingBox.bounds.max.z),
                                     new Vector3(boundingBox.bounds.max.x, boundingBox.bounds.min.y, boundingBox.bounds.max.z),
                                     boundingBox.bounds.max
                                   };
        lc2.SetUp();

        GameObject line3 = Instantiate(linePrefab, transform);
        LineController lc3 = line3.GetComponent<LineController>();
        lc3.points = new Vector3[] { 
                                     boundingBox.bounds.min,
                                     new Vector3(boundingBox.bounds.min.x, boundingBox.bounds.min.y, boundingBox.bounds.max.z)
                                   };
        lc3.SetUp();

        line3 = Instantiate(linePrefab, transform);
        lc3 = line3.GetComponent<LineController>();
        lc3.points = new Vector3[] {
                                     new Vector3(boundingBox.bounds.max.x, boundingBox.bounds.min.y, boundingBox.bounds.min.z),
                                     new Vector3(boundingBox.bounds.max.x, boundingBox.bounds.min.y, boundingBox.bounds.max.z)
                                   };
        lc3.SetUp();

        line3 = Instantiate(linePrefab, transform);
        lc3 = line3.GetComponent<LineController>();
        lc3.points = new Vector3[] {
                                     new Vector3(boundingBox.bounds.max.x, boundingBox.bounds.max.y, boundingBox.bounds.min.z),
                                     boundingBox.bounds.max,
                                   };
        lc3.SetUp();

        line3 = Instantiate(linePrefab, transform);
        lc3 = line3.GetComponent<LineController>();
        lc3.points = new Vector3[] {
                                     new Vector3(boundingBox.bounds.min.x, boundingBox.bounds.max.y, boundingBox.bounds.min.z),
                                     new Vector3(boundingBox.bounds.min.x, boundingBox.bounds.max.y, boundingBox.bounds.max.z)
                                   };
        lc3.SetUp();
    }

    /// <summary>
    /// Set up tags along the x, y and z axis
    /// </summary>
    public void SetUpTags()
    {
        // LABELS

        // x
        GameObject minX = Instantiate(tagPrefab, new Vector3(boundingBox.bounds.min.x, boundingBox.bounds.min.y - 1f * transform.lossyScale.y, boundingBox.bounds.min.z), new Quaternion(), transform);
        TagController minXT = minX.GetComponent<TagController>();
        minXT.SetText("" + minmax[0]);

        GameObject maxX = Instantiate(tagPrefab, new Vector3(boundingBox.bounds.max.x, boundingBox.bounds.min.y - 1f * transform.lossyScale.y, boundingBox.bounds.min.z), new Quaternion(), transform);
        TagController maxXT = maxX.GetComponent<TagController>();
        maxXT.SetText("" + minmax[1]);

        // y
        GameObject minY = Instantiate(tagPrefab, new Vector3(boundingBox.bounds.min.x - 1.5f * transform.lossyScale.x, boundingBox.bounds.min.y, boundingBox.bounds.min.z), new Quaternion(), transform);
        TagController minYT = minY.GetComponent<TagController>();
        minYT.SetText("" + minmax[2]);

        GameObject maxY = Instantiate(tagPrefab, new Vector3(boundingBox.bounds.min.x - 1.5f * transform.lossyScale.x, boundingBox.bounds.max.y, boundingBox.bounds.min.z), new Quaternion(), transform);
        TagController maxYT = maxY.GetComponent<TagController>();
        maxYT.SetText("" + minmax[3]);

        // z
        GameObject minZ = Instantiate(tagPrefab, new Vector3(boundingBox.bounds.min.x - 1.5f * transform.lossyScale.x, boundingBox.bounds.min.y - 1f * transform.lossyScale.y, boundingBox.bounds.min.z), new Quaternion(), transform);
        TagController minZT = minZ.GetComponent<TagController>();
        minZT.SetText("" + minmax[4]);

        GameObject maxZ = Instantiate(tagPrefab, new Vector3(boundingBox.bounds.min.x - 1.5f * transform.lossyScale.x, boundingBox.bounds.min.y - 1f * transform.lossyScale.y, boundingBox.bounds.max.z), new Quaternion(), transform);
        TagController maxZT = maxZ.GetComponent<TagController>();
        maxZT.SetText("" + minmax[5]);

        // in betweens -> every tagSpacing one tag
        int countX = (int)((maxX.transform.position.x - boundingBox.bounds.min.x) / (tagSpacing * transform.lossyScale.x));
        float stepX = (minmax[1] - minmax[0]) / (float)countX;
        InstantiateTags(0, minmax[0], stepX, minX.transform.position.x, maxX.transform.position.x, transform.lossyScale.x);
        
        int countY = (int)((maxY.transform.position.y - boundingBox.bounds.min.y) / (tagSpacing * transform.lossyScale.y));
        float stepY = (minmax[3] - minmax[2]) / (float)countY;
        InstantiateTags(1, minmax[2], stepY, minY.transform.position.y, maxY.transform.position.y, transform.lossyScale.y);

        int countZ = (int)((maxZ.transform.position.z - boundingBox.bounds.min.z) / (tagSpacing * transform.lossyScale.z));
        float stepZ = (minmax[5] - minmax[4]) / (float)countZ;
        InstantiateTags(2, minmax[4], stepZ, minZ.transform.position.z, maxZ.transform.position.z, transform.lossyScale.z);
    }

    /// <summary>
    /// Instantate tag
    /// </summary>
    /// <param name="type"> Type of tag, accepted valeues - 0 (x), 1 (y), 2 (z) </param>
    /// <param name="start"> Start value </param>
    /// <param name="step"> Step in spacing </param>
    /// <param name="from"> Start position </param>
    /// <param name="to"> End position </param>
    private void InstantiateTags(int type, float start, float step, float from, float to, float sizing)
    {
        int times = 1;
        for (float i = from + (tagSpacing * sizing); i < to; i += (tagSpacing * sizing))
        {
            GameObject tag = null;
            if (type == 0)
                tag = Instantiate(tagPrefab, new Vector3(i, boundingBox.bounds.min.y - 1f * sizing, boundingBox.bounds.min.z), new Quaternion(), transform);
            else if (type == 1)
                tag = Instantiate(tagPrefab, new Vector3(boundingBox.bounds.min.x - 1.5f * sizing, i, boundingBox.bounds.min.z), new Quaternion(), transform);
            else if (type == 2)
                tag = Instantiate(tagPrefab, new Vector3(boundingBox.bounds.min.x - 1.5f * sizing, boundingBox.bounds.min.y - 1f * sizing, i), new Quaternion(), transform);

            TagController tagT = tag.GetComponent<TagController>();
            float labelValue = (start + (times * step));
            tagT.SetText(String.Format("{0:0.00}", labelValue));
            times++;
        }
    }

}
