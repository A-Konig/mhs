﻿using System.Collections;
using UnityEngine;

public class DisplayFunctionController : MonoBehaviour
{
    public bool ready;
    public bool error;

    [Header("Components")]
    public GameObject terrainPrefab;
    public GameObject boundingBoxPrefab;

    [Header("Bounding box settings")]
    public float[] boundingBoxSize;

    [Header("Function settings")]
    public string function;
    public bool isDiscontinuous;
    public bool longEdgesExperimental;
    public int[] minmax; // minX maxX minZ maxZ
    public float samplingRate;

    // Start is called before the first frame update
    void Start()
    {
        SetUp();
    }

    /// <summary>
    /// Set up function
    /// </summary>
    public void SetUp()
    {
        ready = false;

        GameObject boundingBox = Instantiate(boundingBoxPrefab, transform);
        GameObject terrain = Instantiate(terrainPrefab, transform);

        BoxCollider collider = boundingBox.GetComponent<BoxCollider>();
        collider.size = new Vector3(boundingBoxSize[0], boundingBoxSize[1], boundingBoxSize[2]);

        TerrainGeneration tg = terrain.GetComponent<TerrainGeneration>();
        tg.func = function;
        tg.minmax = minmax;
        tg.isDiscontinuous = isDiscontinuous;
        tg.longEdgesExperimental = longEdgesExperimental;
        tg.samplingRate = samplingRate;
        tg.boundingBox = boundingBox.GetComponent<BoxCollider>();
        tg.SetUp();

        StartCoroutine(WaitingForGeneration(tg, boundingBox));
    }

    /// <summary>
    /// Waiting for the creation of function surface
    /// </summary>
    /// <param name="tg"> Surface genearator </param>
    /// <param name="boundingBox"> Bounding box </param>
    /// <returns></returns>
    IEnumerator WaitingForGeneration(TerrainGeneration tg, GameObject boundingBox)
    {
        while (!tg.readyForDisplay)
            yield return null;

        BoundingBoxController bc = boundingBox.GetComponent<BoundingBoxController>();
        var minmax = tg.fs.GetMinMaxOfFunction();
        bc.minmax = new int[] { Mathf.FloorToInt(minmax[0]), Mathf.CeilToInt(minmax[1]), Mathf.FloorToInt(minmax[2]), Mathf.CeilToInt(minmax[3]), Mathf.FloorToInt(minmax[4]), Mathf.CeilToInt(minmax[5]) };
        bc.SetUp();

        error = tg.error;
        ready = true;
    }

}
