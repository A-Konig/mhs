﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DiscontinuityLocator
{

    Function function;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="func"> Function </param>
    public DiscontinuityLocator(Function func)
    {
        this.function = func;
    }

    /// <summary>
    /// Find discontinuities in function using sampling with sample of size sample
    /// </summary>
    /// <param name="pointsZ"> Number of points in z </param>
    /// <param name="sample"> Sample size </param>
    /// <param name="fromX"> From point x </param>
    /// <param name="toX"> To point x </param>
    /// <returns> List with list of coordiantes of points with discontinuities [list-with-x-coordinates list-with-z-coordinate] </returns>
    public List<float>[] FindDiscontinuity(int pointsZ, float sample, int fromX, int toX)
    {
        List<float> xList = new List<float>();
        List<float> zList = new List<float>();

        for (int x = fromX; x < toX; x++)
        {
            float xVal = x * sample;

            for (int z = 0; z < pointsZ; z++)
            {
                float zVal = z * sample;

                double y = function.EvaluateFunction(xVal, zVal);

                /*
                double yDXMinus = function.EvaluateFunction(xVal - sample, zVal);
                double yDZMinus = function.EvaluateFunction(xVal, zVal - sample);
                double diffX = (yDXMinus- y);
                double diffZ = (yDZMinus- y);
                */

                // cannot be computed -> discontinuity
                if (double.IsInfinity(y) || double.IsNegativeInfinity(y) ) // || Math.Abs(diffX) > 2 || Math.Abs(diffZ) > 2)
                {
                    // only note down the ones we haven't seen yet
                    if ((!xList.Contains(xVal) ) && !zList.Contains(zVal))
                    {
                        xList.Add(xVal);
                        zList.Add(zVal);

                        Debug.Log(xVal + " " + zVal);
                        double y2 = function.EvaluateFunction(xVal, zVal);
                        double y3 = function.EvaluateFunction(5.01f, 0.01f);

                        Debug.Log("value" + y2);
                        Debug.Log(((int)((function.minmax[0] + xVal) * 100)) / 100.0f);
                        Debug.Log(((int)((function.minmax[2] + zVal) * 100)) / 100.0f);
                        Debug.Log("value" + y3);
                    }
                }
            }
        }

        return new List<float>[] { xList, zList };
    }

}
