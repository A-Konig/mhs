﻿using AXLibrary;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Function
{
    internal Expression expression;
    internal int[] minmax;
    internal bool isDiscontinuous;

    public Function(Expression expression, int[] minmax, bool isDiscontinuous)
    {
        this.expression = expression;
        this.minmax = minmax;
        this.isDiscontinuous = isDiscontinuous;
    }

    public double EvaluateFunction(float xVal, float zVal)
    {
        ExpressionContext context = new ExpressionContext();
        context.Bind(Variable.x, minmax[0] + xVal); // ((int)((minmax[0] + xVal) * 100)) / 100.0f); // 
        context.Bind(Variable.z, minmax[2] + zVal); // ((int)((minmax[2] + zVal) * 100)) / 100.0f); //

        return expression.Evaluate(context);
    }

    internal double EvaluateFunctionNoRound(float xVal, float zVal)
    {
        ExpressionContext context = new ExpressionContext();
        context.Bind(Variable.x, minmax[0] + xVal); // 
        context.Bind(Variable.z, minmax[2] + zVal); // 

        return expression.Evaluate(context);
    }
}
