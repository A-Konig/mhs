﻿using UnityEngine;

public interface IFunctionSolver
{
     void GenerateGeometry();
     void Preprocess();
     void Postprocess();
     float[] GetMinMaxOfFunction();
     void RemapToBox(Collider boundingBox, Transform parent);
     void GenerateTriangles(int pointsX, int pointsZ);
}
