﻿using Delaunay;
using Delaunay.Geo;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FunctionSampler : IFunctionSolver
{
    [Header("Function")]
    Function func;
    float samplingRate;
    internal int[] minmax;
    internal double maxY, minY;

    [Header("Data for mesh")]
    public Vector3[] points;
    public Vector2[] uv;
    public int[] indices;

    [Header("Lists")]
    List<Vector3> pointsList;
    List<Vector2> uvList;
    public List<Vector3> lines;

    [Header("Variables")]
    float diffTolerance = 0.5f;
    double lastY; // last computed y

    internal List<LineSegment> m_delaunayTriangulation;

    public FunctionSampler(Function func, float samplingRate, int[] minmax)
    {
        this.func = func;
        this.samplingRate = samplingRate;
        this.minmax = minmax;
    }

    public void GenerateGeometry()
    {
        // generate
        GeneratePoints();
        // GenerateTriangles(new Vector3(), new Vector3());
    }

    public void GenerateTriangles(int pointsX, int pointsZ)
    {
        List<Vector2> points2D = new List<Vector2>();
        List<uint> colors = new List<uint>();

        for (int i = 0; i < points.Length; i++)
        {
            points2D.Add(new Vector2(points[i].x, points[i].z));
            colors.Add(50);
        }
        
        // Triangulator t = new Triangulator(points2D);
        // indices = t.Triangulate();

        Voronoi v = new Voronoi(points2D, colors, new Rect(minmax[0], minmax[1], minmax[2], minmax[3]));
        //Voronoi v = new Voronoi(points2D, colors, new Rect(min.x, min.z, max.x-min.x, max.z-min.z));
        m_delaunayTriangulation = v.DelaunayTriangulation();

        /*
        int[] indices2 = new int[2 * indices.Length];
        for (int i = 0; i < indices.Length; i++)
        {
            indices2[i] = indices[i];
            indices2[(indices2.Length - 1) - i] = indices[i];
        }

        indices = indices2;
        */

    }

    private void GeneratePoints()
    {
        // max and min for y axis
        maxY = double.MinValue;
        minY = double.MaxValue;

        // front face points (including doubles)
        pointsList = new List<Vector3>();
        uvList = new List<Vector2>();

        float step = samplingRate;
        float maxStep = samplingRate;
        float minStep = 0.1f;

        bool lastDivided = false;
        bool divided = false;
        bool lastEnlarged = false;
        bool enlarged = false;

        float currentX = minmax[0];
        while (currentX <= minmax[1])
        {
            int count = InsertRow(currentX);

            // compute difference from last row -> divide step until satisfied
            double y = func.EvaluateFunction(currentX - minmax[0] + step, 0);
            double yDX = func.EvaluateFunction(currentX - minmax[0], 0);
            double diffX = yDX - y;

            enlarged = false;
            divided = false;

            // enlarge
            if (Math.Abs(diffX) < diffTolerance) // && !lastDivided)
            {
                while (true)
                {
                    y = func.EvaluateFunction(currentX - minmax[0] + step, 0);
                    yDX = func.EvaluateFunction(currentX - minmax[0], 0);
                    diffX = yDX - y;

                    if (Math.Abs(diffX) >= diffTolerance)
                        break;

                    if (step > maxStep)
                    {
                        step = maxStep;
                        break;
                    }

                    step *= 2.0f;
                    enlarged = true;
                }
            }
            // split
            else // if (!lastEnlarged)
            {
                while (true)
                {
                    y = func.EvaluateFunction(currentX - minmax[0] + step, 0);
                    yDX = func.EvaluateFunction(currentX - minmax[0], 0);
                    diffX = yDX - y;

                    if (Math.Abs(diffX) < diffTolerance)
                        break;

                    if (step < minStep)
                    {
                        step = minStep;
                        break;
                    }

                    step /= 2.0f;
                    divided = true;
                }
            }

            lastDivided = divided;
            lastEnlarged = enlarged;
            currentX += step;
        }

        points = pointsList.ToArray();
    }

    private int InsertRow(float currentX)
    {
        int pointCount = 0;
        float currentZ = minmax[2];
        
        float step = samplingRate;
        float maxStep = samplingRate;
        float minStep = 0.1f;

        bool lastDivided = false;
        bool divided = false;
        bool lastEnlarged = false;
        bool enlarged = false;

        while (currentZ <= minmax[3])
        {
            InsertPoint(currentX - minmax[0], currentZ - minmax[2]);
            pointCount++;

            // compute difference from last row -> divide step until satisfied
            double y = func.EvaluateFunction(currentX - minmax[0], currentZ - minmax[2] + step);
            double yDX = func.EvaluateFunction(currentX - minmax[0], currentZ - minmax[2]);
            double diffX = yDX - y;

            enlarged = false;
            divided = false;

            // enlarge
            if (Math.Abs(diffX) < diffTolerance && !lastDivided)
            {
                while (true)
                {
                    y = func.EvaluateFunction(currentX - minmax[0], currentZ - minmax[2] + step);
                    yDX = func.EvaluateFunction(currentX - minmax[0], currentZ - minmax[2]);
                    diffX = yDX - y;

                    if (Math.Abs(diffX) >= diffTolerance)
                        break;

                    if (step > maxStep)
                    {
                        step = maxStep;
                        break;
                    }

                    step *= 2.0f;
                    enlarged = true;
                }
            }
            // split
            else if (!lastEnlarged)
            {
                while (true)
                {
                    y = func.EvaluateFunction(currentX - minmax[0], currentZ - minmax[2]);
                    yDX = func.EvaluateFunction(currentX - minmax[0], currentZ - minmax[2] - step);
                    diffX = yDX - y;

                    if (Math.Abs(diffX) < diffTolerance)
                        break;

                    if (step < minStep)
                    {
                        step = minStep;
                        break;
                    }

                    step /= 2.0f;
                    divided = true;
                }
            }

            lastDivided = divided;
            lastEnlarged = enlarged;
            currentZ += step;
        }

        return pointCount;
    }

    private void InsertPoint(float x, float z)
    {
        double y = func.EvaluateFunction(x, z);

        bool adjusted = false;
        if (double.IsInfinity(y) || double.IsNegativeInfinity(y))
        {
            adjusted = true;
            y = lastY + 1;
        }

        pointsList.Add(new Vector3(x, (float)y, z));
        uvList.Add(new Vector2(0, 1));

        lastY = y;

        if (!adjusted)
        {
            if (minY > y) minY = y;
            if (maxY < y) maxY = y;
        }
    }

    /// <summary>
    /// Remap function to fit bounding box
    /// </summary>
    /// <param name="boundingBox"> Bounding box </param>
    /// <param name="parent"> Parent transform </param>
    public void RemapToBox(Collider boundingBox, Transform parent)
    {
        // map to bounding box location
        double minX = minmax[0];
        double maxX = minmax[1];
        double minZ = minmax[2];
        double maxZ = minmax[3];

        Vector3 min = boundingBox.bounds.min;
        Vector3 max = boundingBox.bounds.max;

        for (int i = 0; i < points.Length; i++)
        {
            float newX = (float)((points[i].x - 0) / (maxX - minX) * (max.x - min.x) + min.x);
            float newY = (float)((points[i].y - minY) / (maxY - minY) * (max.y - min.y) + min.y);
            float newZ = (float)((points[i].z - 0) / (maxZ - minZ) * (max.z - min.z) + min.z);
            points[i] = parent.InverseTransformPoint(new Vector3(newX, newY, newZ));
        }
    }

    /// <summary>
    /// Preprocess function not needed here
    /// </summary>
    public void Preprocess()
    {
        // no preprocess needed
    }

    /// <summary>
    /// Get minmax of function
    /// </summary>
    /// <returns> Array with minmax [minX maxX minY maxY minZ maxZ] </returns>
    public float[] GetMinMaxOfFunction()
    {
        return new float[] { minmax[0], minmax[1], (float)minY, (float)maxY, minmax[2], minmax[3] };
    }

    /// <summary>
    /// Post process
    /// </summary>
    public void Postprocess()
    {
        CreateLines();
    }

    /// <summary>
    /// Create lines in 3D from lines in 2D generated by Delaunay triangulation
    /// </summary>
    private void CreateLines()
    {
        List<Vector3> pointsSorted = SortPoints(points);
        lines = new List<Vector3>();

        if (m_delaunayTriangulation != null)
        {
            for (int i = 0; i < m_delaunayTriangulation.Count; i++)
            {
                Vector2 left = (Vector2)m_delaunayTriangulation[i].p0;
                Vector2 right = (Vector2)m_delaunayTriangulation[i].p1;
                lines.Add(FindPoint(new Vector3(left.x, 0, left.y), pointsSorted));
                lines.Add(FindPoint(new Vector3(right.x, 0, right.y), pointsSorted));
            }
        }
    }

    /// <summary>
    /// Find corresponding 3D point to a 2D point (point with 0 y coordinate)
    /// </summary>
    /// <param name="point2D"> Point in 2D </param>
    /// <param name="points3D"> Points in 3D </param>
    /// <returns> Corresponding 3D point </returns>
    private Vector3 FindPoint(Vector3 point2D, List<Vector3> points3D)
    {
        // find starting point
        int indexX = BinarySearchIterative(points3D.ToArray(), point2D);

        // start searching to the left and right of found starting index
        int indexLeft = indexX, indexRight = indexX, add = 0;
        while ((indexLeft >= 0 && indexLeft < points3D.Count) || (indexRight >= 0 && indexRight < points3D.Count))
        {
            if ((indexLeft >= 0 && indexLeft < points3D.Count) && (Math.Abs(points3D[indexX - add].x - point2D.x) < 0.1 && Math.Abs(points3D[indexX - add].z - point2D.z) < 0.1))
                return points3D[indexX - add];

            if ((indexRight >= 0 && indexRight < points3D.Count) && (Math.Abs(points3D[indexX + add].x - point2D.x) < 0.1 && Math.Abs(points3D[indexX + add].z - point2D.z) < 0.1))
                return points3D[indexX + add];

            add++;
            indexLeft = indexX - add;
            indexRight = indexX + add;
        }

        return new Vector3();
    }

    /// <summary>
    /// Binary search in points sorted by x and z
    /// </summary>
    /// <param name="inputArray"> Input sorted array of points </param>
    /// <param name="key"> Point we're looking for </param>
    /// <returns> Index of first found point with the same x </returns>
    public static int BinarySearchIterative(Vector3[] inputArray, Vector3 key)
    {
        int min = 0;
        int max = inputArray.Length - 1;
        while (min <= max)
        {
            int mid = (min + max) / 2;
            if (Math.Abs(key.x - inputArray[mid].x) < 0.1)
            {
                return ++mid;
            }
            else if (key.x < inputArray[mid].x)
            {
                max = mid - 1;
            }
            else
            {
                min = mid + 1;
            }
        }
        return -1;
    }

    /// <summary>
    /// Sort array of points by x and z coordinates
    /// </summary>
    /// <param name="points"> Input array </param>
    /// <returns> Sorted list </returns>
    private List<Vector3> SortPoints(Vector3[] points)
    {
        List<Vector3> l = new List<Vector3>();

        for (int i = 0; i < points.Length; i++)
            l.Add(points[i]);

        List<Vector3> sorted = l.OrderBy(x => x.x).ThenBy(x => x.z).ToList();
        return sorted;
    }
}
