﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineController : MonoBehaviour
{
    public LineRenderer lr;
    internal Vector3[] points;

    // Start is called before the first frame update
    void Start()
    {
    }

    public void SetUp()
    {

        lr.positionCount = points.Length;
        lr.startWidth = 0.1f * transform.lossyScale.x;
        lr.endWidth = 0.1f * transform.lossyScale.x;
        for (int i = 0; i < points.Length; i++)
            lr.SetPosition(i, points[i]);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
