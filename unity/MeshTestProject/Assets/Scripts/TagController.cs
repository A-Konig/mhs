﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TagController : MonoBehaviour
{
    public TextMeshPro text;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward, Camera.main.transform.up);
    }

    public void SetText(string text)
    {
        this.text.text = text;
    }

}
