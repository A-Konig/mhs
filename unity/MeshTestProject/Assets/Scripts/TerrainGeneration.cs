﻿using UnityEngine;
using AXLibrary;
using System.Collections;
using System.Threading;
using System;
using System.Collections.Generic;
using System.Linq;

public class TerrainGeneration : MonoBehaviour
{
    internal bool readyForDisplay;
    internal bool error;

    [Header("Components")]
    public Collider boundingBox;

    [Header("Function")]
    public string func;
    public bool isDiscontinuous;
    public bool longEdgesExperimental;
    public int[] minmax; // minX maxX minZ maxZ calculating Y
    public float samplingRate;
    internal IFunctionSolver fs;
    Expression expression;

    [Header("Mesh")]
    Mesh mesh;

    public void SetUp()
    {
         GenerateFunctionDisplayDoubleSampler();
        // GenerateFunctionDisplaySampler();
    }

    /// <summary>
    /// Generate function terrain using FunctionSampler
    /// </summary>
    private void GenerateFunctionDisplaySampler()
    {
        error = false;
        readyForDisplay = false;

        AXParser parser = new AXParser();
        try
        {
            expression = parser.Parse(func);

            ExpressionContext context = new ExpressionContext();
            context.Bind(Variable.x, 0);
            context.Bind(Variable.z, 0);
            double testVal = expression.Evaluate(context);
        }
        catch
        {
            Debug.LogError("Wrong function!!!");
            error = true;
            return;
        }

        Function f = new Function(expression, minmax, isDiscontinuous);
        fs = new FunctionSampler(f, samplingRate, minmax);
        
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        StartCoroutine(GenerateGizmosTerrainDelaunay());
    }

    /// <summary>
    /// Generate gizmos display of functions using FunctionSampler, coroutine
    /// </summary>
    /// <returns></returns>
    IEnumerator GenerateGizmosTerrainDelaunay()
    {
        Thread t = new Thread(() =>
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            fs.Preprocess();
            
            stopwatch.Start();
            fs.GenerateGeometry();
            stopwatch.Stop();
            Debug.Log("time = " + stopwatch.ElapsedMilliseconds);

        });

        t.Start();

        while (t.IsAlive)
            yield return null;

        t.Join();

        fs.RemapToBox(boundingBox, transform);

        Thread t2 = new Thread(() =>
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            
            stopwatch.Start();
            fs.GenerateTriangles(0, 0);
            fs.Postprocess();
            stopwatch.Stop();
            
            Debug.Log("time = " + stopwatch.ElapsedMilliseconds);

        });

        t2.Start();

        while (t2.IsAlive)
            yield return null;

        t2.Join();

        // create empty mesh
        mesh.Clear();
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        mesh.vertices = ((FunctionSampler)fs).points;
        mesh.triangles = ((FunctionSampler)fs).indices;
        mesh.uv = ((FunctionSampler)fs).uv;
        mesh.RecalculateNormals();

        readyForDisplay = true;
    }

    /// <summary>
    /// Generate function terrain using FunctionDoubleSampler
    /// </summary>
    private void GenerateFunctionDisplayDoubleSampler()
    {
        readyForDisplay = false;

        AXParser parser = new AXParser();
        try
        {
            expression = parser.Parse(func);

            ExpressionContext context = new ExpressionContext();
            context.Bind(Variable.x, 0); 
            context.Bind(Variable.z, 0); 
            double testVal = expression.Evaluate(context);
        }
        catch
        {
            Debug.LogError("Wrong function!!!");
            error = true;
            return;
        }

        fs = new FunctionDoubleSampler(expression, minmax, samplingRate, isDiscontinuous, longEdgesExperimental);

        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        StartCoroutine(GenerateTerrain());
    }

    /// <summary>
    /// Generate function terrain coroutine, using FunctionDoubleSampler
    /// </summary>
    /// <returns></returns>
    IEnumerator GenerateTerrain()
    {
        Thread t = new Thread( () => 
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            
            if (isDiscontinuous)
            {
                stopwatch.Start();
                fs.Preprocess();
                stopwatch.Stop();
                Debug.Log("time = " + stopwatch.ElapsedMilliseconds);
            }

            stopwatch.Start();
            fs.GenerateGeometry();
            stopwatch.Stop();
            Debug.Log("time = " + stopwatch.ElapsedMilliseconds);

            stopwatch.Start();
            fs.Postprocess();
            stopwatch.Stop();

            Debug.Log("time = " + stopwatch.ElapsedMilliseconds);
        });

        t.Start();

        while (t.IsAlive)
            yield return null;

        t.Join();

        fs.RemapToBox(boundingBox, transform);
        UpdateMeshDoubleSampler();
        readyForDisplay = true;
    }

    /// <summary>
    /// Update mesh values using FunctionDoubleSampler
    /// </summary>
    public void UpdateMeshDoubleSampler()
    {
        mesh.Clear();
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        mesh.vertices = ((FunctionDoubleSampler)fs).points;
        mesh.triangles = ((FunctionDoubleSampler)fs).indices;
        mesh.uv = ((FunctionDoubleSampler)fs).uv;
        mesh.RecalculateNormals();
    }

    /// <summary>
    /// Debug output
    /// </summary>
    private void OnDrawGizmos()
    {
        if (fs == null || (fs is FunctionDoubleSampler && ((FunctionDoubleSampler)fs).points == null) || (fs is FunctionSampler && ((FunctionSampler)fs).points == null)) return;

        Vector3[] points = null; 
        if (fs is FunctionDoubleSampler)
        {
            points = ((FunctionDoubleSampler)fs).points;
        }
        if (fs is FunctionSampler)
        {
            points = ((FunctionSampler)fs).points;

            Gizmos.color = Color.magenta;
            if (((FunctionSampler)fs).lines != null)
            {
                for (int i = 0; i < ((FunctionSampler)fs).lines.Count - 1; i += 2)
                {
                    Gizmos.DrawLine(transform.TransformPoint(((FunctionSampler)fs).lines[i]), transform.TransformPoint(((FunctionSampler)fs).lines[i + 1]));
                }
            }
        }

        // points
        Gizmos.color = Color.white;
        for (int i = 0; i < points.Length; i++)
        {
            // Gizmos.DrawSphere(transform.TransformPoint(points[i]), 0.05f);
        }
    }

}
